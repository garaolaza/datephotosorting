#!/usr/bin/python

#    Copyright 2013 Gari Araolaza gari@eibar.org
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


# I try to sort all the pictures from my phone in date based folders
# like: YYYYMMDD
# This script creates the folders and sorts the files in each folder
#
# Dependencies: You will need gexiv2 library installed on your system to run it
#
#
# Usage:  python datephotosorting.py <SOURCE_PATH> <DESTINATION_PATH>
#
# Please give feedback if you find something could be better in this script.
# gari@eibar.org
 

import sys, os, shutil
from gi.repository import GExiv2


def get_images_from_path(basepath):
    image_list = []
    for root, subdirs, files in os.walk(basepath):
        for file in files:
            if os.path.splitext(file)[1].lower() in ('.jpg', '.jpeg'):
                image_list.append(os.path.join(root, file))
    return image_list
       
def save_file_sorted(image_path, dest_path):
    exif = GExiv2.Metadata(image_path)
    try:
        day = exif['Exif.Image.DateTime'][:10].replace(':','')
    except:
        try:
            day = exif['Exif.Photo.DateTimeOriginal'][:10].replace(':','')
        except:
            day ='00000000'

    filename = os.path.basename(image_path)
    full_dir = os.sep.join([dest_path,day])
    
    #check if folder doesn't exist. Then create it.
    
    print 'FULL_DIR:', full_dir
    if not os.path.exists(full_dir):
        os.mkdir(full_dir)

    full_path = os.sep.join([full_dir,filename])
    #check if filename exists
    if os.path.exists(full_path):
        print 'Overwriting', full_path
    else:
        shutil.copyfile(image_path,full_path)
        print 'Copied:',image_path
        print 'To:', full_path

    return

def main(argv):
    if len(argv)<2:
        print """This program sorts jpg images in folders, based on their date."""
        return 
    
    image_list = get_images_from_path(argv[1])
    dest_path = argv[2]  
    
    for image_path in image_list:
        print image_path
        save_file_sorted(image_path, dest_path)


if __name__ == "__main__":
    main(sys.argv)